; This script allows language files to be written in an easier format
; than the nsis default

; Usage of the new LangString macro:
; !insertmacro LANG_LOAD <Language> (see list at http://pastebin.com/raw/JyV531w9)
; ${LangString} (un.)<Name> <String>
; ${VersionKey} <key> <value>
; The LangString / VersionKey will use the language you defined in LangLoad

; Language init macro called at the top of lang files
!macro LANG_LOAD LANGUAGE
  !ifdef LangString
    !undef LangString
    !undef VersionKey
  !endif

  !define LangString "!insertmacro LANG_LOAD_LANG_STRING ${LANGUAGE}"
  !define VersionKey "!insertmacro LANG_LOAD_VI_ADDVERSIONKEY ${LANGUAGE}"
!macroend

; Macro to be called for defining language strings
!macro LANG_LOAD_LANG_STRING LANGUAGE NAME STRING
  LangString "${NAME}" "${LANG_${LANGUAGE}}" "${STRING}"
!macroend

; Macro wrapper for VIAddVersionKey
!macro LANG_LOAD_VI_ADDVERSIONKEY LANGUAGE KEY VALUE
  VIAddVersionKey "/LANG=${LANG_${LANGUAGE}}" "${KEY}" "${VALUE}"
!macroend

; Include all language files
!include ".\languages\*.nsh"

; Clean up
!undef LangString
