# ges-updater

This is a simple program that checks for, downloads, and executes update packages for GoldenEye: Source. It can be run on demand by the user, or run by GoldenEye: Source itself if an update is available.

The actual heavy lifting of installing is handled by [ges-installer](https://gitlab.com/soupcan/ges-installer/), and in fact shares much of the same infrastructure with it. This program simply checks for the update, downloads the latest available update program, verifies it and then runs it.

The program checks for updates using a text file, the documentation for which can be found [here](https://gitlab.com/soupcan/ges-installer/-/blob/master/docs/updater.md) in the ges-installer repository.

## Logging

The updater logs its activity to `%TEMP%\gesource-updater-<timestamp>.log`

## Overrides and switches

The updater's behavior can be modified using either an overrides file or the command line. This is helpful for debugging or testing new updates before release. If an override is specified in both the overrides file and on the command line, the command line takes precedence.

### Overrides file

The overrides file is read from the same directory and same file name (substituting `.exe` for `.txt`). For example, to create an overrides file for `gesource_update.exe`, create a file in the same directory named `gesource_update.txt`.

The overrides file will have a parent key named `updater_overrides`, with the overrides specified below it. For example:

```
updater_overrides
{
	MODDIR gesource
}
```

### Command-line parameters

To specify parameters on the command line, use `/OVERRIDE_NAME=VALUE`, such as:

```
gesource_update.exe /MODDIR=gesource
```

### Override list

Currently supported overrides:

* `MODDIR`: Name of the mod directory. This is used to check the registry for uninstall data created by `ges-installer`
* `MINOR_VERSION`: Minor version, e.g. 5.0 or 5.1, that this updater is intended to be used for. The updater will error out if the minor version in the registry differs from this.
* `UPDATE_FILE_URL`: Link to the update.txt file
* `SIGNATURE_CHECK_ENABLE`: Set to 0 to disable signature checking. If this setting is unset, or if it's set to any other value, signature checking is enabled.